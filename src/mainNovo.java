import java.util.Collections;

import AG.Config;
import AG.Individuo;
import AG.Populacao;
import AG.Solucao;
import AGMO.NSGAII;
import Atrativos.Valores;

public class mainNovo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Valores.init();

		// System.out.println(Valores.ATRATIVOS);

		Solucao solucao = new Solucao(null);
		// solucao.setFitnessAdequado(9);
		int tamPopulacao = Config.TAM_POP;
		int numGenes = Config.NUM_GENES;
		Populacao populacao = new Populacao();
		Populacao Q, pUq;
		String res = "";

		populacao.Incializar(tamPopulacao, numGenes);
		Populacao populacaoBase = new Populacao(populacao);
		System.out.println("Popula��o Base:\n" + populacaoBase);

		// Inicializa��o
		populacao.Avaliar(solucao);

		// Collections.sort(populacao.getIndividuos(),
		// Individuo.Comparators.PARETO);
		Collections.sort(populacao.getIndividuos());
		System.out.println("Popula��o Primeira avalia��o:\n" + populacao);
		res += printPareto(populacao);

		for (int i = 0; i < Config.MAX_GERACOES; i++) {

			
			// Gerar Popula��o Q
			Q = populacao.novaGeracao();
			populacao.setGeracao(Q.getGeracao());

			pUq = Populacao.unir(populacao, Q);
			pUq.Avaliar(null);
			Collections.sort(pUq.getIndividuos(), Individuo.Comparators.PARETO);
//			Collections.sort(pUq.getIndividuos());
//			System.out.println("Popula��o pareto:\n" + pUq);

			populacao.setIndividuos(pUq.getIndividuos().subList(0,populacao.getIndividuos().size()));
//			Collections.sort(populacao.getIndividuos());
			populacao.Avaliar(null);
			
			if (i == Config.MAX_GERACOES/4) {
				res += printPareto(populacao);
				System.out.println("Popula��o 1 quarto:\n" + populacao);
			}
			if (i == Config.MAX_GERACOES/2) {
				res += printPareto(populacao);
				System.out.println("Popula��o 1 metade:\n" + populacao);
			}
//			System.out.println("Popula��o 1 metade:\n" + populacao);

		}
		System.out.println("Popula��o Ultima avalia��o:\n" + populacao);
		res += printPareto(populacao);
		
		System.out.println(res);

	}
	
	public static String printPareto(Populacao populacao){
		String res = "";
		
		for (Individuo individuo : populacao.getIndividuos()) {
			res += individuo.getfDistancia().intValue()+","+individuo.getfPreco().intValue()+","+individuo.getFrentePareto()+"\n";
		}
		res += "\n--------------------------------------\n";
		return res;
	}

}
