import AG.Config;
import AG.Populacao;
import AG.Solucao;
import Atrativos.Valores;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Valores.init();

//		System.out.println(Valores.ATRATIVOS);

		Solucao solucao = new Solucao(null);
		// solucao.setFitnessAdequado(9);
		int tamPopulacao = Config.TAM_POP;
		int numGenes = Config.NUM_GENES;
		Populacao populacao = new Populacao();

		populacao.Incializar(tamPopulacao, numGenes);

		populacao.Avaliar(solucao);		
		
		Populacao populacaoBase = new Populacao(populacao);
		
		System.out.println("Popula��o Base:\n" + populacaoBase);

		int melhorFitness = 0;
		int fitnessAnterior = 0;
		int seguidos = 1;

		for (int i = 1; seguidos < 30 ; i++) {
			System.out.println("--------Gera��o #"+i+"--------");
			System.out.println(populacao.getMelhorIndividuo());
//			 System.out.println("Popula��o na itera��o #"+i+" :\n"+populacao);
			//populacao.Avaliar(solucao);
			populacao.setGeracao(populacao.getGeracao() + 1);
			populacao = new Populacao();

			if (populacao.getMelhorFitness() > melhorFitness) {
				melhorFitness = populacao.getMelhorFitness();
			}
			
			if (populacao.getMelhorFitness() == fitnessAnterior) {
				seguidos++;
				//if(seguidos == 5) break;
			}else{
				seguidos = 1;
				fitnessAnterior = populacao.getMelhorFitness();
			}
		}
		
		System.out.println("Popula��o Final:\n" + populacao);
		
		
		System.out.println(Solucao.traduzirIndividuo(populacao.getMelhorIndividuo()));

	}

}
