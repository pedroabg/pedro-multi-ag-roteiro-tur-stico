package testes;

import java.util.*;

import AG.Config;
import AG.Fitness;
import AG.Solucao;

public class CasoTeste {
	
	Execucao[] execucoes = new Execucao[5];	
	List<List<int[]>> listaDeListas = new ArrayList<List<int[]>>();  
	
	
	public void executar(){
		
		
		for (int i = 0; i < execucoes.length; i++) {
			execucoes[i] = new Execucao();
			execucoes[i].executar();
		
			listaDeListas.add(execucoes[i].getGraf());
		}
//		csv.gerar(listaDeListas, this.toString());
		csv.gerar(listaDeListas,"");

		
	}
	
	


	@Override
	public String toString() {
		String retorno = "CasoTeste \n";
		retorno += "\n Pd:"+Config.Pd+" \n";
		retorno += "\n Pp:"+Config.Pp+" \n";
		retorno += "\n Pa:"+Config.Pa+" \n";
		retorno += "\n Pt:"+Config.Pt+" \n";

		for (int i = 0; i < execucoes.length; i++) {
			retorno += execucoes[i];
			
		}
		
//		retorno += "Popula��o Inicial:\n"+execucoes[0].getPopulacaoInicial();
//		retorno += "Popula��o Final:\n"+execucoes[0].getPopulacaoFinal();
		retorno += "Inicial: "+ Fitness.calcAtributos(execucoes[0].getPopulacaoInicial().getMelhorIndividuo());
		
		retorno += "\n\nFinal: "+Fitness.calcAtributos(execucoes[0].getPopulacaoFinal().getMelhorIndividuo());
		
		retorno += "\n\nSolu��o Inicial: "+ execucoes[0].getPopulacaoInicial().getMelhorIndividuo();
		retorno += " Final:\n"+execucoes[0].getPopulacaoFinal().getMelhorIndividuo();
		
		retorno += "Solu��o Inicial:\n"+ Solucao.traduzirIndividuo(execucoes[0].getPopulacaoInicial().getMelhorIndividuo());
		retorno += "Solu��o Final:\n"+Solucao.traduzirIndividuo(execucoes[0].getPopulacaoFinal().getMelhorIndividuo());

		retorno += "Pop inicial"+execucoes[0].getPopulacaoInicial();
		retorno += "Pop final"+execucoes[0].getPopulacaoFinal();
		
		
		return retorno;
	}




	public Execucao[] getExecucoes() {
		return execucoes;
	}


	public void setExecucoes(Execucao[] execucoes) {
		this.execucoes = execucoes;
	}
	
	

}
