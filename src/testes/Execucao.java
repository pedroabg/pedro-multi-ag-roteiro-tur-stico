package testes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import AG.*;
import Atrativos.Valores;

public class Execucao {

	Solucao solucao = new Solucao(null);
	int tamPopulacao = Config.TAM_POP;
	int numGenes = Config.NUM_GENES;
	Populacao populacao = new Populacao();
	Populacao populacaoInicial, populacaoFinal;
	int melhorFitness = 0;
	int numGeracoes = 0;
	Individuo melhorIndividuo;
	int[] geracaoFitnes ;
	List<int[]> graf = new ArrayList<int[]>();
	private Populacao Q, pUq;

	public void executar() {
		
		

		
		System.out.println("e");

		populacao.Incializar(tamPopulacao, numGenes);

		// System.out.println(populacao);
		populacao.Avaliar(null);
		this.populacaoInicial = new Populacao(populacao);
		
		geracaoFitnes = new int[3+4];
		geracaoFitnes[0] = 0;
		geracaoFitnes[1] = populacao.getMelhorDistancia();
		geracaoFitnes[2] = populacao.getPiorDistancia();
		geracaoFitnes[3] = populacao.getMelhorPreço();
		geracaoFitnes[4] = populacao.getPiorPreco();
		geracaoFitnes[5] = 1;
		geracaoFitnes[6] = 1;
	
		graf.add(geracaoFitnes);
		
		
			
		int fitnessAnterior = 0;
		int seguidos = 1;

		for (int i = 1; ( i< Config.MAX_GERACOES) ; i++) {
			numGeracoes = i;

		
			
			
			Q = populacao.novaGeracao();
			populacao.setGeracao(Q.getGeracao());

			pUq = Populacao.unir(populacao, Q);
			pUq.Avaliar(null);
			Collections.sort(pUq.getIndividuos(), Individuo.Comparators.PARETO);
//			Collections.sort(pUq.getIndividuos());
//			System.out.println("População pareto:\n" + pUq);

			populacao.setIndividuos(pUq.getIndividuos().subList(0,populacao.getIndividuos().size()));
//			Collections.sort(populacao.getIndividuos());
			populacao.Avaliar(null);
			
			

			geracaoFitnes = new int[3+4];
			geracaoFitnes[0] = i;
			geracaoFitnes[1] = populacao.getMelhorDistancia();
			geracaoFitnes[2] = populacao.getPiorDistancia();
			geracaoFitnes[3] = populacao.getMelhorPreço();
			geracaoFitnes[4] = populacao.getPiorPreco();
			geracaoFitnes[5] = 1;
			geracaoFitnes[6] = 1;

			graf.add(geracaoFitnes);
			
			if (populacao.getMelhorFitness() > melhorFitness) {
				this.melhorFitness = populacao.getMelhorFitness();
			}
			
			if (populacao.getMelhorFitness() == fitnessAnterior) {
				seguidos++;
			}else{
				seguidos = 1;
				fitnessAnterior = populacao.getMelhorFitness();
			}
		}
		
		this.populacaoFinal = new Populacao(populacao);
		this.melhorIndividuo = this.populacaoFinal.getMelhorIndividuo();
		
//		System.out.println("População Final:\n" + populacao);
		
		
		
	}
	
	
	
	





	@Override
	public String toString() {
		return "Ex: G:"
				+ numGeracoes + ", Melhor Individuo: " + melhorIndividuo + " \n";
	}





	public Individuo getMelhorIndividuo() {
		return melhorIndividuo;
	}



	public void setMelhorIndividuo(Individuo melhorIndividuo) {
		this.melhorIndividuo = melhorIndividuo;
	}



	public Solucao getSolucao() {
		return solucao;
	}

	public void setSolucao(Solucao solucao) {
		this.solucao = solucao;
	}

	public int getTamPopulacao() {
		return tamPopulacao;
	}

	public void setTamPopulacao(int tamPopulacao) {
		this.tamPopulacao = tamPopulacao;
	}

	public int getNumGenes() {
		return numGenes;
	}

	public void setNumGenes(int numGenes) {
		this.numGenes = numGenes;
	}

	public Populacao getPopulacao() {
		return populacao;
	}

	public void setPopulacao(Populacao populacao) {
		this.populacao = populacao;
	}

	public Populacao getPopulacaoInicial() {
		return populacaoInicial;
	}

	public void setPopulacaoInicial(Populacao populacaoInicial) {
		this.populacaoInicial = populacaoInicial;
	}

	public Populacao getPopulacaoFinal() {
		return populacaoFinal;
	}

	public void setPopulacaoFinal(Populacao populacaoFinal) {
		this.populacaoFinal = populacaoFinal;
	}

	public int getMelhorFitness() {
		return melhorFitness;
	}

	public void setMelhorFitness(int melhorFitness) {
		this.melhorFitness = melhorFitness;
	}

	public int getNumGeracoes() {
		return numGeracoes;
	}

	public void setNumGeracoes(int numGeracoes) {
		this.numGeracoes = numGeracoes;
	}









	public int[] getGeracaoFitnes() {
		return geracaoFitnes;
	}









	public void setGeracaoFitnes(int[] geracaoFitnes) {
		this.geracaoFitnes = geracaoFitnes;
	}









	public List<int[]> getGraf() {
		return graf;
	}









	public void setGraf(List<int[]> graf) {
		this.graf = graf;
	}





}
