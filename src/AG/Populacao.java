package AG;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import AGMO.NSGAII;

public class Populacao {

	List<Individuo> individuos;

	int geracao;

	public Populacao() {
		this.geracao = 1;
		individuos = new ArrayList<Individuo>();
	}

	public Populacao(int geracao) {
		this.geracao = geracao;
		individuos = new ArrayList<Individuo>();
	}
	
	
	public Populacao(Populacao populacao1) {
		
//		public ArrayList(Collection<? extends E> c)
//		Constructs a list containing the elements of the specified collection, in the order they are returned by the collection's iterator.
		this.individuos = new ArrayList<Individuo>(populacao1.copyIndividuos());
		this.geracao = 0;
	}
	
	public List<Individuo> copyIndividuos(){
		List<Individuo> individuos = this.getIndividuos();
		List<Individuo> clones = new ArrayList<Individuo>();
		for (Individuo individuo : individuos) {
			clones.add(new Individuo(individuo));
		}
		
		return clones;
		
	}

	
	public void Incializar(int quantidade, int numGenes) {
		individuos = new ArrayList<Individuo>();
		for (int i = 0; i < quantidade; i++) {
			individuos.add(Individuo.GerarIndividuo(numGenes, 1));

		}

	}

	public void Avaliar(Solucao solucao) {

		int fitness;
//		System.out.println(this);
		for (Individuo individuo : this.getIndividuos()) {
			fitness = 0;
			//System.out.println("---------------"+individuo);
			individuo.avaliar();
		}
		//Collections.sort(this.individuos);
		NSGAII.calculateOnlyFitness(this);

	}

	public Populacao novaGeracao() {
		Populacao populacao = new Populacao(this);
		Populacao novaGeracao = new Populacao(this);
		
		//System.out.println("Popula��o entrando em nova geracao "+this);
		Populacao novosMembros = new Populacao(populacao.getGeracao());
		novosMembros = cruzar(populacao);
		
		//System.out.println(novosMembros);
		novosMembros = novosMembros.mutar(novosMembros);
		//System.out.println("Ap�s muta��o:\n"+novosMembros);
		//novosMembros.Avaliar(null);
		//System.out.println("Ap�s avaliar:\n"+novosMembros);
		
		//System.out.println("this:\n"+this+"\nPopulacao:\n"+populacao);
		
		novaGeracao.selecionarSobrevicentes(novosMembros);
		
		novaGeracao.setGeracao(this.getGeracao()+1);
		
		return novaGeracao;
	}

	private void selecionarSobrevicentes(Populacao novosMembros) {
		Populacao cheia = new Populacao(this);
		//System.out.println(this);
		List<Individuo> individuos = cheia.copyIndividuos();
		
		individuos.addAll(novosMembros.copyIndividuos());
		cheia.setIndividuos(individuos);
		cheia.Avaliar(null);
		individuos = cheia.copyIndividuos();
		
//		Collections.sort(individuos);
		Collections.sort(individuos, Individuo.Comparators.PARETO);
		//System.out.println("Sorted\n"+individuos);
		
		for (int i = 0; i < novosMembros.getIndividuos().size(); i++) {
			individuos.remove(individuos.size()-1);
		}
		
		//System.out.println("Depois de remover\n"+individuos);
		
		this.setIndividuos(individuos);
		
	}

	private Populacao cruzar(Populacao populacao) {
		Populacao popCruzar = new Populacao();
		popCruzar = Operators.sampleTorneio(populacao);
		
		popCruzar = Operators.cruzar(popCruzar);
		
		//Descobrir pq est� mudando!!!
		//System.out.println("this ap�s cruzamento "+populacao);
		return popCruzar;

	}

	private Populacao mutar(Populacao novosMembros) {
		Populacao populacao = new Populacao(novosMembros);
		List<Individuo> individuos = novosMembros.copyIndividuos();
		for (Individuo individuo : individuos) {
			//System.out.println("antes de mutar:"+individuo);
			individuo.checaMutacao();
			//System.out.println("depois de mutar:"+individuo);
		}
		novosMembros.setIndividuos(individuos);
		return novosMembros;

	}

	private List<Individuo> selecao(Populacao velha, int numGenes) {
		List<Individuo> individuos = new ArrayList<Individuo>();

		for (int i = 0; i < velha.individuos.size(); i++) {
			if (i < velha.individuos.size() / 2)
				individuos.add(this.individuos.get(i));
			else
				individuos.add(Individuo.GerarIndividuo(numGenes,
						this.getGeracao()));

		}
		return individuos;

	}

	public int getPiorFitness() {
		
		return individuos.get(individuos.size()-1).getFitness();
	}
	public int getMelhorFitness() {

		return individuos.get(0).getFitness();
	}
	public Individuo getMelhorIndividuo() {
		
		return individuos.get(0);
	}

	public int getGeracao() {
		return geracao;
	}

	public void setGeracao(int geracao) {
		this.geracao = geracao;
	}

	public List<Individuo> getIndividuos() {
		return individuos;
	}

	public void setIndividuos(List<Individuo> individuos) {
		this.individuos = individuos;
	}

	@Override
	public String toString() {
		return "Populacao [individuos=\n" + individuos + "]";
	}

	public static Populacao getSample(Populacao populacao, int size) {
		Populacao sample = new Populacao();
		List<Individuo> individuos = new ArrayList<Individuo>(populacao.getIndividuos());
		Collections.shuffle(individuos);
		
		sample.setIndividuos(individuos.subList(0, size));  
		
		return sample;
	}

	public static Populacao unir(Populacao populacao, Populacao Q) {
		// TODO Auto-generated method stub
		Populacao total = new Populacao();
		
		for (Individuo ind : populacao.getIndividuos()) {
			total.getIndividuos().add(ind);
		}
		for (Individuo ind : Q.getIndividuos()) {
			total.getIndividuos().add(ind);
		}
		
		return new Populacao(total);
	}
	
	public int getMelhorDistancia() {
		// TODO Auto-generated method stub
		List<Individuo> list = new ArrayList<Individuo>();
		list = this.copyIndividuos();
		
		Collections.sort(list, Individuo.Comparators.DISTANCIA);
		return list.get(0).getfDistancia().intValue();
	}
	public int getPiorDistancia() {
		// TODO Auto-generated method stub
		List<Individuo> list = new ArrayList<Individuo>();
		list = this.copyIndividuos();
		
		Collections.sort(list, Individuo.Comparators.DISTANCIA);
		return list.get(list.size()-1).getfDistancia().intValue();
	}

	public int getMelhorPre�o() {
		// TODO Auto-generated method stub
		List<Individuo> list = new ArrayList<Individuo>();
		list = this.copyIndividuos();
		
		Collections.sort(list, Individuo.Comparators.PRECO);
		return list.get(0).getfPreco().intValue();
	}
	public int getPiorPreco() {
		// TODO Auto-generated method stub
		List<Individuo> list = new ArrayList<Individuo>();
		list = this.copyIndividuos();
		
		Collections.sort(list, Individuo.Comparators.PRECO);
		return list.get(list.size()-1).getfPreco().intValue();
	}
	
	
	


}
