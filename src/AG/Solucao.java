package AG;

import java.util.ArrayList;
import java.util.List;

public class Solucao {

	String alvo;
	int fitnessAdequado;
	boolean solucaoAceita;

	public Solucao(String alvo) {
		super();
		this.alvo = alvo;
		solucaoAceita = false;
	}

	public String getAlvo() {
		return alvo;
	}

	public void setAlvo(String alvo) {
		this.alvo = alvo;
	}

	public boolean isSolucaoAceita(Populacao populacao) {

		return solucaoAceita;
	}

	public int getFitnessAdequado() {
		return fitnessAdequado;
	}

	public void setFitnessAdequado(int fitnessAdequado) {
		this.fitnessAdequado = fitnessAdequado;
	}

	public boolean isSolucaoAceita() {
		return solucaoAceita;
	}

	public void setSolucaoAceita(boolean solucaoAceita) {
		this.solucaoAceita = solucaoAceita;
	}

	public static String traduzirIndividuo(Individuo ind) {
		List<String> Turnos = new ArrayList<String>();
		Turnos.add("Manha") ;Turnos.add("Tarde") ; Turnos.add("Noite") ; 
		
		String tabela = "";
		String res = "";
//		tabela += "-------------------------------------\n";
//		tabela += "|turno/dia| Segunda | Ter�a | Quarta |\n";
//		tabela += "-------------------------------------\n";

		ind.decodificaAtrativo();
		
		
		for (int j = 0; j < 3; j++) {
			res+=" "+ Turnos.get(j) +"";
			for (int i = j; i < ind.getAtrativos().length; i += 3) {
				res += " ; "+ ind.getAtrativos()[i].getNome()+" ";

//				res += "\n| Manh� |" + ind.getGenoma(i)[j++] + "\n";
//				res += "| Tarde |" + ind.getGenoma()[j++] + "\n";
//				res += "| Noite |" + ind.getGenoma()[j] + "\n";
			}
			res+="\n";
		}

		return tabela + res;

	}

}
