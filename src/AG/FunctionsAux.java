package AG;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FunctionsAux {
	
	
	
	public static List<Integer> gerarListaOrdenada(int tam){
		List<Integer> ret = new ArrayList<Integer>();
		
		for (int i = 0; i < tam; i++) {
			ret.add(i);
		}
		
		return ret;
		
	}
	public static List<Integer> gerarListaEmbaralhada(int tam){
		List<Integer> ret = new ArrayList<Integer>();
		
		ret = gerarListaOrdenada(tam);
		
		Collections.shuffle(ret);
		
		return ret;
		
	}
	public static List<Integer> getRandomSample(int t , int tam){
		List<Integer> ret = new ArrayList<Integer>();
		
		ret = gerarListaEmbaralhada(tam);
		
		return ret.subList(0, t);
		
	}
	public static void arraycopy(List<Individuo> individuos, int firstPos,
			Individuo[] I, int j, int length) {
		
		for (int k = 0; k < length; k++) {
			I[k] = individuos.get(firstPos + k);
		}
		
		// TODO Auto-generated method stub
		
	}
	

}
