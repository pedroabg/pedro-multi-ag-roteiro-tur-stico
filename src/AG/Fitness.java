package AG;

import java.util.List;

import Atrativos.Distancias;
import Atrativos.TipoAtrativo;
import Atrativos.Usuario;

public class Fitness {




	public static int penalizar(Individuo you) {
		int bonus = 0;
		int seqCoop = 0;

		int[] cadeia = you.getGenoma();

		for (int i = 0; i < cadeia.length; i++) {
			for (int j = 0; j < cadeia.length; j++) {
				if (cadeia[i] == cadeia[j] && i != j) {
					bonus -= 50;
					// System.out.println("opa " + cadeia[i] + cadeia[j]);
				}
			}
		}

		return bonus;

	}


	public static double calcularPercurso(Individuo ind) {
		// TODO Auto-generated method stub
		double dist = 0;
		int id;
		double d;
		int idPosHosp = 21;

		for (int i = 0; i < ind.getAtrativos().length; i++) {
			// hosp,i + i,i+1 + i+1,i+2 + i+2 hosp
			// ind.getAtrativos()[i];

			d = Distancias.DISTANCIAS[idPosHosp][ind.getAtrativos()[i].getId() - 1];
			dist += d;
			// System.out.println(d);
			d = Distancias.DISTANCIAS[ind.getAtrativos()[i].getId() - 1][ind
					.getAtrativos()[++i].getId() - 1];
			dist += d;
			// System.out.println(d);
			d = Distancias.DISTANCIAS[ind.getAtrativos()[i].getId() - 1][ind
					.getAtrativos()[++i].getId() - 1];
			dist += d;
			// System.out.println(d);
			d = Distancias.DISTANCIAS[ind.getAtrativos()[i].getId() - 1][idPosHosp];
			dist += d;
			// System.out.println(d);

			// System.out.println(" d:"+dist);

		}

		return dist*1;
	}

	public static double calcularPreco(Individuo ind) {

		double preco = 0;

		for (int i = 0; i < ind.getAtrativos().length; i++) {

			preco += ind.getAtrativos()[i].getPreco();

		}

		return preco;
	}

	public static double calcularAvaliacao(Individuo ind) {

		double avaliacao = 0;

		for (int i = 0; i < ind.getAtrativos().length; i++) {

			avaliacao += (ind.getAtrativos()[i].getAvaliacao() - 3);

		}
		// media
		// System.out.println(ind+" Avalia��o: "+ avaliacao);

		avaliacao = avaliacao / ind.getAtrativos().length;
		// Porcentagem visto que o m�ximo � 5

		avaliacao = (100 * avaliacao) / 2;

		return avaliacao;
	}

	public static double calcularCompatibilidade(Individuo ind) {
		double compatibilidade = 0;
		List<TipoAtrativo> tags;

		for (int i = 0; i < ind.getAtrativos().length; i++) {

			tags = ind.getAtrativos()[i].getTags();
			compatibilidade += calcularCompatibilidade(tags);

		}

		compatibilidade /= Usuario.PREFERENCIAS.size();

		return compatibilidade;
	}

	private static double calcularCompatibilidade(List<TipoAtrativo> tags) {
		// TODO Auto-generated method stub
		List<TipoAtrativo> utags = Usuario.PREFERENCIAS;
		double x = 0;
		int i = 0;

		for (TipoAtrativo utag : utags) {
			if (tags.contains(utag)) {
				x += (tags.size() - tags.indexOf(utag)) + (utags.size() - utags.indexOf(utag));
			} else {
				x +=  - (utags.size() - utags.indexOf(utag));
			}

		}

		return x;
	}

	public static String calcAtributos(Individuo melhorIndividuo) {
		// TODO Auto-generated method stub
		String res = "";

	
		melhorIndividuo.decodificaAtrativo();

		double[] a = { Fitness.calcularPercurso(melhorIndividuo),
				Fitness.calcularPreco(melhorIndividuo),
				Fitness.calcularCompatibilidade(melhorIndividuo),
				Fitness.calcularAvaliacao(melhorIndividuo) };

		res = "Dist�ncia percorrida: " + a[0]
				+ "km, gasto: R$" + a[1] + ", Compatibilidade: " + a[2]
				+ ", Avalia��o:" + a[3] + "%";

		return res;
	}

}
