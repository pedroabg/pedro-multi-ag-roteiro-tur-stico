package AG;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Atrativos.Atrativo;
import Atrativos.Valores;

public class Individuo implements Comparable<Individuo> {

	int fitness;
	int[] genoma;
	// 1 coopera 0 n�o coopera
	Atrativo[] atrativos;
	int geracao;
	Populacao populacao;
	Double fDistancia;
	Double fPreco;
	Double fAvaliacao;
	Double fCompatibilidade;
	int frentePareto;
	int crowdingDistance;

	static MersenneTwisterFast prng = new MersenneTwisterFast();

	double probMutacaco;

	public int getFitness() {
		return fitness;
	}

	public void setFitness(int fitness) {
		this.fitness = fitness;
	}

	public int[] getGenoma() {
		return genoma;
	}

	public void setGenoma(int[] genoma) {
		this.genoma = genoma;
	}

	public Individuo() {
		super();
		fitness = -1;
		probMutacaco = 0.4;
		crowdingDistance = 0;

	}

	static Individuo GerarIndividuo(int numGenes, int geracao) {

		Individuo individuo = new Individuo();
		individuo.genoma = new int[numGenes];
		individuo.setGeracao(geracao);
		
		List<Integer> genesAleatorios = gerarGenesAleatorios(); 

		for (int i = 0; i < numGenes; i++) {

			individuo.genoma[i] = genesAleatorios.get(i);
		}

		
		// individuo.populacao = populacao;

		return individuo;
	}

	private static List<Integer> gerarGenesAleatorios() {
		// TODO Auto-generated method stub
		List<Integer> genesAleatorios = new ArrayList<Integer>();
		
		for (int i = 1; i <= Config.NUM_ALFABETO; i++) {
			genesAleatorios.add(i);
		}
		Collections.shuffle(genesAleatorios);
		return genesAleatorios;
	}






	public int compareTo(Individuo o) {
		// TODO Auto-generated method stub

		return o.getFitness() - this.fitness;
	}

	public int getGeracao() {
		return geracao;
	}

	public void setGeracao(int geracao) {
		this.geracao = geracao;
	}



	public void avaliar() {
		Double finalFitness = new Double(0);
		double D, P, T, A;		
		this.decodificaAtrativo();
		int n = this.getAtrativos().length;
		
		this.fDistancia = Fitness.calcularPercurso(this);
		// invers�o. botar 1/distancia m�dia
		D = (this.fDistancia/n)*Config.Pd;
//		this.fDistancia = D;
		fPreco = Fitness.calcularPreco(this) ;
		P = (fPreco/n)*Config.Pp;
//		fPreco = P;
		
		fCompatibilidade = Fitness.calcularCompatibilidade(this);
		T = (fCompatibilidade/n)*Config.Pt;
//		fCompatibilidade = T;
	
		fAvaliacao = Fitness.calcularAvaliacao(this);
		A = (fAvaliacao)*Config.Pa;
		
//		fAvaliacao = A;
		
		finalFitness += (D + P + T + A) ;		
		
		finalFitness += Fitness.penalizar(this);
		
		
		//this.fitness = finalFitness.intValue();
	
	}

	public int checaFitness(Individuo parceiro) {
		int fitness = 0;
		int gene1, gene2;

		return fitness;
	}

	public void checaMutacao() {
		int dado;
		int[] cadeia = this.genoma;

		for (int i = 0; i < cadeia.length; i++) {
			if (isMutavel()) {
//				 System.out.println("muta��o");
				cadeia[i] = mutacao(cadeia);

			}
		}

		this.genoma = cadeia;

	}

	private int mutacao(int[] cadeia) {
		
		int novoValor = prng.nextInt(Config.NUM_ALFABETO) + 1;
		for (int i = 0; i < cadeia.length; i++) {
			if(novoValor == cadeia[i]){
				novoValor = prng.nextInt(Config.NUM_ALFABETO) + 1;
				i = 0;
//				System.out.println("repetido");
			}
		}
		return novoValor;
	}

	private boolean isMutavel() {
		// Numero sorteado entre 0 e 100,
		// se ele for menor que a probabilidade significa que
		// que o sorteio caiu dentro da probabilidade
		int dado = prng.nextInt(100);
		// System.out.println(dado);
		// System.out.println((Config.PM * 100));
		return dado <= (Config.PM * 100);
	}
	
	


	public Individuo(Individuo other) {
		super();
		this.frentePareto = other.getFrentePareto();
		this.crowdingDistance = other.getCrowdingDistance();
		this.fitness = other.getFitness();
		this.genoma = other.getGenoma().clone();
		this.geracao = other.getGeracao();
		this.fDistancia = other.fDistancia;
		this.fPreco = other.fPreco;
		this.fCompatibilidade = other.fCompatibilidade;
		this.fAvaliacao = other.fAvaliacao;
	}

	@Override
	public String toString() {
		return "" + Arrays.toString(genoma) + " , Pre�o total: "+fPreco+" , Dist�ncia: "+fDistancia
				+" F:"+frentePareto+" , crowdD: "+crowdingDistance+"  Fit: "+fitness+" \n";
//				+ ", D = "+fDistancia+"Km, P = R$"+fPreco+" Com = "+fCompatibilidade+" A = "+fAvaliacao+" \n";
		// + Arrays.toString(cadeiaDecodificada)+ "\n";
	}
	
	public void decodificaAtrativo(){
		Atrativo[] atrativos = new Atrativo[genoma.length];
		for (int i = 0; i < genoma.length; i++) {
			atrativos[i] = Valores.ATRATIVOS.get(genoma[i]-1);
		}
		
		this.atrativos = atrativos;
	}

	public Atrativo[] getAtrativos() {
		return atrativos;
	}

	public void setAtrativos(Atrativo[] atrativos) {
		this.atrativos = atrativos;
	}

	public Double getDistanciaTotal() {
		return fDistancia;
	}

	public void setDistanciaTotal(Double distanciaTotal) {
		this.fDistancia = distanciaTotal;
	}

	public Double getPrecoTotal() {
		return fPreco;
	}

	public void setPrecoTotal(Double precoTotal) {
		this.fPreco = precoTotal;
	}

	public Double getAvaliacaoTotal() {
		return fAvaliacao;
	}

	public void setAvaliacaoTotal(Double avaliacaoTotal) {
		this.fAvaliacao = avaliacaoTotal;
	}

	public Double getCompatibilidadeTotal() {
		return fCompatibilidade;
	}

	public void setCompatibilidadeTotal(Double compatibilidadeTotal) {
		this.fCompatibilidade = compatibilidadeTotal;
	}

	public Double getfDistancia() {
		return fDistancia;
	}

	public void setfDistancia(Double fDistancia) {
		this.fDistancia = fDistancia;
	}

	public Double getfPreco() {
		return fPreco;
	}

	public void setfPreco(Double fPreco) {
		this.fPreco = fPreco;
	}

	public Double getfAvaliacao() {
		return fAvaliacao;
	}

	public void setfAvaliacao(Double fAvaliacao) {
		this.fAvaliacao = fAvaliacao;
	}

	public Double getfCompatibilidade() {
		return fCompatibilidade;
	}

	public void setfCompatibilidade(Double fCompatibilidade) {
		this.fCompatibilidade = fCompatibilidade;
	}
	
	
	 public static class Comparators {


	        public static Comparator<Individuo> PRECO = new Comparator<Individuo>() {
	            @Override
	            public int compare(Individuo o1, Individuo o2) {
	                return (int)(100*o1.getfPreco() - 100*o2.getfPreco());
	            }
	        };
	        public static Comparator<Individuo> DISTANCIA = new Comparator<Individuo>() {
	        	@Override
	        	public int compare(Individuo o1, Individuo o2) {
	        		return (int)(100*o1.getfDistancia() - 100*o2.getfDistancia());
	        	}
	        };
	        public static Comparator<Individuo> PARETO = new Comparator<Individuo>() {
	            @Override
	            public int compare(Individuo o1, Individuo o2) {
	            	if(o1.getFrentePareto() != o2.getFrentePareto())
	            	 return (int)(100*o1.getFrentePareto() - 100*o2.getFrentePareto());
	            	else
	            		return -1*(o1.getCrowdingDistance() - o2.getCrowdingDistance());
	            }
	        };
	    }


	public boolean dominates(Individuo ind2) {		
		// verificar se this � melhor que ind2 em pelo menos um objetivo		
		
	
		Individuo ind1 = this;

		
//		x domina y
//		A solu��o x � pelo menos igual a y em todas fun��es;
//		A solu��o x � superior a y em pelo menos uma fun��o objetivo.
		
		
		//condi��o um
		
		if((ind1.getfPreco() <= ind2.getfPreco()) && (ind1.getfDistancia() <= ind2.getfDistancia())){
//			A solu��o x � pelo menos igual a y em todas fun��es;
			if((ind1.getfPreco() < ind2.getfPreco()) || (ind1.getfDistancia() < ind2.getfDistancia())){
				return true;
			}
			
		}
		

//		System.out.println("\nfalse");
		
		return false;
	}

	public int getFrentePareto() {
		return frentePareto;
	}

	public void setFrentePareto(int frentePareto) {
		this.frentePareto = frentePareto;
	}

	public int getCrowdingDistance() {
		return crowdingDistance;
	}

	public void setCrowdingDistance(int crowdingDistance) {
		this.crowdingDistance = crowdingDistance;
	}

	public int getObjective(int obj) {
		switch (obj) {
		case 0:
			return ((Double)fPreco).intValue();
		case 1:
			return ((Double)fDistancia).intValue();

		default:
			break;
		}
		return 0;
	}
	
	
}
