package AG;
import java.util.Collections;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Solucao solucao = new Solucao("917856765");
		// solucao.setFitnessAdequado(9);
		int tamPopulacao = Config.TAM_POP;
		int numGenes = Config.NUM_GENES;
		Populacao populacao = new Populacao();
		Populacao populacaoBase;

		populacao.Incializar(tamPopulacao, numGenes);

		// System.out.println(populacao);
		populacao.Avaliar(solucao);
		populacaoBase = new Populacao(populacao);
		System.out.println("Popula��o Base:\n" + populacaoBase);

		int melhorFitness = 0;
		int fitnessAnterior = 0;
		int seguidos = 1;

		for (int i = 1; (seguidos < 100 && i<10) ; i++) {
//			System.out.println("--------Gera��o #"+i+"--------");
			 System.out.println("Popula��o na itera��o #"+i+" :\n"+populacao);
			//populacao.Avaliar(solucao);
			populacao.setGeracao(populacao.getGeracao() + 1);
			populacao = new Populacao();
//			System.out.println("\nPopula��o final na itera��o #" + i + " :\n"
//					+ populacao);
//			System.out.println("#"+i+": Melhor fitness nessa gera��o: "
//					+ populacao.getMelhorFitness() + " || Melhor fitness at� agora: "+melhorFitness);
			

			//System.out.println(i+","+populacao.getMelhorFitness());
			
			if (populacao.getMelhorFitness() > melhorFitness) {
				melhorFitness = populacao.getMelhorFitness();
			}
			
			if (populacao.getMelhorFitness() == fitnessAnterior) {
				seguidos++;
				//if(seguidos == 5) break;
			}else{
				seguidos = 1;
				fitnessAnterior = populacao.getMelhorFitness();
			}
		}
		
		System.out.println("Popula��o Final:\n" + populacao);



	}

}
