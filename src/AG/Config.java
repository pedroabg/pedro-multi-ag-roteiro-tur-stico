package AG;

import java.util.ArrayList;
import java.util.List;

import Atrativos.Valores;

public class Config {
	
	public static final int INFINITO = 99999999;
	public static int TAM_POP = 20;
	public static int NUM_GENES = 9;
	public static int NUM_ALFABETO = Valores.ATRATIVOS.size();
	
	
	//Peso na distancia percorrida, i.e, a importancia do tamanho do percurso/ med 130
	public static double FEd = 1.00;
	public static double Pd = 1.0 * FEd;
	
	//Peso do pre�o, i.e, a importancia do pre�o do percurso / med 197
	public static double FEp = 100.0;
	public static double Pp = 1.0 * FEp;
	
	
	
	//Peso da avalia��o, i.e, a importancia do nota da atra��o / med 1
	public static double FEa = 1;
	public static double Pa = 0.0 * FEa;
	
	//Peso da Compatibilidade, i.e, a importancia das tags do atrativo casarem com o perfil do usu�rio / 90
	public static double FEt = 25;
	public static double Pt = 0.0 * FEt;
	
	
	//quantas gera��es o melhor fitness deve permanecer inalteradom para o algoritimo parar	
	public static int K = 100;

	public static int MAX_GERACOES = 300;
	
	

	

	//Porcentagem de pais selecionados para cruzamento; setar de 5% a 20%
	public static double LAMBIDA_fator = 0.8;
	public static int LAMBIDA = (int) (TAM_POP * LAMBIDA_fator);
	
	//Quantidade de participantes do torneio. Deve ser menor que lambida
	public static int RING = 2;
	
	//Porcentagem de genes que ir�o cruzar; setar de 5% a 20%
	public static double PGC_fator = 0.50;
	public static int PGC = (int) (NUM_GENES * PGC_fator);
	
	public static double PM = 0.3;
	
	public static double PCruzamento = 0.5;
	
	
	//Tamanho da cadeia de bonus
	public static int BONUS_SEQ = 3;
	public static double PESO_DO_BONUS = 1.0;
	
	

}
















