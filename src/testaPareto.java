import java.util.Collections;

import AG.Config;
import AG.Individuo;
import AG.Populacao;
import AG.Solucao;
import AGMO.NSGAII;
import Atrativos.Valores;


public class testaPareto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Valores.init();

		// System.out.println(Valores.ATRATIVOS);

		Solucao solucao = new Solucao(null);
		// solucao.setFitnessAdequado(9);
		int tamPopulacao = Config.TAM_POP;
		int numGenes = Config.NUM_GENES;
		Populacao populacao = new Populacao();

		populacao.Incializar(tamPopulacao, numGenes);

		
		populacao.Avaliar(solucao);

		Populacao populacaoBase = new Populacao(populacao);

		System.out.println("Popula��o Base:\n" + populacaoBase);
		
//		Collections.sort(populacao.getIndividuos());
//		System.out.println("Popula��o ordenada:\n" + populacao);
//		
//		Collections.sort(populacao.getIndividuos(), Individuo.Comparators.DISTANCIA);
//		System.out.println("Popula��o distancia:\n" + populacao);
//		
//		Collections.sort(populacao.getIndividuos(), Individuo.Comparators.PRECO);
//		System.out.println("Popula��o ordenada pre�o:\n" + populacao);
		
		populacao.getIndividuos().get(0).setfPreco(45.0); populacao.getIndividuos().get(0).setfDistancia(67.0);//f1 
		populacao.getIndividuos().get(1).setfPreco(47.1); populacao.getIndividuos().get(1).setfDistancia(65.0);//f2 
		populacao.getIndividuos().get(2).setfPreco(58.1); populacao.getIndividuos().get(2).setfDistancia(35.3);//f3 
		populacao.getIndividuos().get(3).setfPreco(58.0); populacao.getIndividuos().get(3).setfDistancia(35.7);//f2 
		populacao.getIndividuos().get(4).setfPreco(60.0); populacao.getIndividuos().get(4).setfDistancia(30.0);//f1 
		populacao.getIndividuos().get(5).setfPreco(61.0); populacao.getIndividuos().get(5).setfDistancia(69.5);//f1 
		populacao.getIndividuos().get(6).setfPreco(69.0); populacao.getIndividuos().get(6).setfDistancia(47.7);//f1 
		populacao.getIndividuos().get(7).setfPreco(73.0); populacao.getIndividuos().get(7).setfDistancia(37.0);//f1 
		populacao.getIndividuos().get(8).setfPreco(94.0); populacao.getIndividuos().get(8).setfDistancia(47.3);//f1 
		
		NSGAII.calculaPareto(populacao);
		Collections.sort(populacao.getIndividuos(), Individuo.Comparators.PARETO);
		System.out.println("Popula��o pareto:\n" + populacao);
		
	}

}
