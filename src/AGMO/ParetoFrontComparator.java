package AGMO;

import java.util.Comparator;

public class ParetoFrontComparator {

	 public int compare(Chromosome o1, Chromosome o2) {
         return o1.paretoFront - o2.paretoFront;
     }

	public Comparator<Chromosome> ParetoFrontComparator() {
		
		return PF;
	}
	
	 public static Comparator<Chromosome> PF = new Comparator<Chromosome>() {
        
         public int compare(Chromosome o1, Chromosome o2) {
             return o1.getParetoFront() - o2.getParetoFront();
         }
     };
}
