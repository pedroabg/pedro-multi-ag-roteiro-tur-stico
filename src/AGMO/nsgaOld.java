//package AGMO;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.List;
//
//import AG.Config;
//import AG.Individuo;
//import AG.Populacao;
//
//public class nsgaOld {
//
//
//	
//		// Entrada � um conjunto de solu��es; conjunto de cromossomos
//		static public void fastNonDominatedSort(Chromosome chromosomes[]) {
//			// Cria array de domination do tamanho da popula��o
//			Domination S[] = Domination.createDomination(chromosomes.length);
//			ArrayList<Integer> F = new ArrayList<Integer>();
//
//			for (int p = 0; p < chromosomes.length; p++) {
//				for (int q = 0; q < chromosomes.length; q++) {
//					if (p == q)
//						continue;
//					// para cada solu��o p diferente de q ; no slide i != j
//					if (chromosomes[p].dominates(chromosomes[q]))
//						S[p].dominated.add(q); // sol. p dominates sol. q
//					else if (chromosomes[q].dominates(chromosomes[p]))
//						S[p].dominationCount++; // sol. q dominates sol. p
//				}
//				if (S[p].dominationCount == 0) {
//					chromosomes[p].setParetoFront(1);
//					F.add(p);
//				}
//			}
//
//			int i = 1; // i is the front counter
//			while (!F.isEmpty()) {
//				ArrayList<Integer> Q = new ArrayList<Integer>();
//				for (Integer p : F) {
//					for (Integer q : S[p].dominated) {
//						S[q].dominationCount--;
//						if (S[q].dominationCount == 0) {
//							chromosomes[q].setParetoFront(i + 1);
//							Q.add(q);
//						}
//					}
//				}
//				i++;
//				F = Q;
//			}
//		}
//	
//		private void calculateCrowdingDistances(Chromosome chromosomes[]) {
//			// Linha 1 do slide; seta distancia das solu��es com 0
//			for (int i = 0; i < chromosomes.length; i++)
//				chromosomes[i].setCrowdingDistance(0);
//	
//			Collections.sort(Arrays.asList(chromosomes), ParetoFrontComparator.PF);
//			int firstParetoFront = chromosomes[0].getParetoFront();
//			int lastParetoFront = chromosomes[chromosomes.length - 1]
//					.getParetoFront();
//			int firstPos = 0, size = 0;
//	
//			// para cada frente de pareto; se tiver 3 frentes i vai de 1 a 3
//			for (int i = firstParetoFront; i <= lastParetoFront; i++) {
//				// pega o tamanho da frente de pareto i
//				size = getParetoFrontSize(chromosomes, firstPos, i);
//				// cria um array de solu��es de tamanho size, nele ficar�o todas as
//				// solu��es da frente i
//				Chromosome I[] = new Chromosome[size];
//				System.arraycopy(chromosomes, firstPos, I, 0, I.length);
//	
//				// se h� mais de um elemento nessa frente
//				if (size > 1) {
//					// pra cada objetivo,
//					for (int obj = 0; obj < Constants.NOBJECTIVES; obj++) {
//						// pra cada objetivo, ordena as solu��es da frente corrente
//						// com base nesse objetivo
//						// exemplo: se o objetivo corrente � pre�o, ordena os
//						// individuos por pre�o
//						Collections.sort(Arrays.asList(I),
//								FactoryComparator.getComparator(obj));
//	
//						// lembrando que I � o array de solu��es dessa frente
//						// aqui os extremos est�o sendo setados como infinito
//						I[0].setCrowdingDistance(Double.POSITIVE_INFINITY);
//						I[I.length - 1]
//								.setCrowdingDistance(Double.POSITIVE_INFINITY);
//						// percorre as solu��es tirando a primeira: 0 e a ultima:
//						// length-1 ; pois tem distancia infinita
//						for (int j = 1; j < I.length - 1; j++) {
//							I[j].setCrowdingDistance(I[j].getCrowdingDistance()
//									+ I[j + 1].getObjective(obj)
//									- I[j - 1].getObjective(obj));
//						}
//					}
//	
//				} else {
//					I[0].setCrowdingDistance(Double.POSITIVE_INFINITY);
//				}
//	
//				firstPos += size;
//			}
//		}
//
//		// posi��o inicial da solu��o ; n�mero da camada de pareto
//		public static int getParetoFrontSize(List<Individuo> individuos,
//				int firstPos, int paretoFront) {
//			int count = 0;
//			// ele come�a a contar da primeira posi��o daquela frente; ex: a partir
//			// da posi��o 10 todos seriam frente 2
//			// isso porque os genes j� est�o ordenados pelo atributo frente
//			for (int i = firstPos; i < individuos.size(); i++) {
//				if (individuos.get(i).getFrentePareto() != paretoFront)
//					break;
//				count++;
//			}
//			return count;
//		}
//
//		public void calculateFitness(Chromosome chromosomes[]) {
//			Chromosome.calculateAttributes(chromosomes);
//			NSGAII.fastNonDominatedSort(chromosomes);
//			this.calculateCrowdingDistances(chromosomes);
//			// Finally calculates fitness of all chromosomes
//			for (int i = 0; i < chromosomes.length; i++) {
//				chromosomes[i].setFitness((1f / chromosomes[i].getParetoFront())
//						+ chromosomes[i].getCrowdingDistance());
//			}
//		}
//
//		public void doSelection(Chromosome chromosomes[],
//				Chromosome selectedChromosomes[]) {
//			Chromosome all[] = new Chromosome[Constants.NPOPULATION * 2];
//			for (int i = 0; i < Constants.NPOPULATION; i++) {
//				all[i] = chromosomes[i];
//				all[Constants.NPOPULATION + i] = selectedChromosomes[i];
//			}
//
//			NSGAII.fastNonDominatedSort(all);
//			// Collections.sort(Arrays.asList(all), new ParetoFrontComparator());
//
//			Chromosome survivors[] = new Chromosome[Constants.NPOPULATION];
//
//			int i = 1, firstPos = 0, size = 0, missingChromosomes = Constants.NPOPULATION;
//
//			while (missingChromosomes != 0) {
//				size = getParetoFrontSize(all, firstPos, i);
//				Chromosome F[] = new Chromosome[size];
//				System.arraycopy(all, firstPos, F, 0, F.length);
//
//				this.calculateCrowdingDistances(F);
//
//				if (size <= missingChromosomes) {
//					System.arraycopy(F, 0, survivors, firstPos, F.length);
//					missingChromosomes -= size;
//				} else {
//					// Collections.sort(Arrays.asList(F), new
//					// CrowdingDistanceComparator());
//					System.arraycopy(F, 0, survivors, firstPos, missingChromosomes);
//					missingChromosomes = 0;
//				}
//				firstPos += size;
//				i++;
//			}
//
//			System.arraycopy(survivors, 0, chromosomes, 0, Constants.NPOPULATION);
//			// this is done because one solution may change its pareto front
//			// position or its crowding distance
//			this.calculateOnlyFitness(chromosomes);
//		}
//
//		private void calculateOnlyFitness(Chromosome chromosomes[]) {
//			for (int i = 0; i < Constants.NPOPULATION; i++)
//				chromosomes[i].setFitness((1f / chromosomes[i].getParetoFront())
//						+ chromosomes[i].getCrowdingDistance());
//		}
//
//		private static class Domination {
//			public int dominationCount = 0;
//			public ArrayList<Integer> dominated = new ArrayList<Integer>();
//
//			public static Domination[] createDomination(int size) {
//				Domination d[] = new Domination[size];
//				for (int i = 0; i < size; i++)
//					d[i] = new Domination();
//				return d;
//			}
//		}
//
//		// //////////////////////////////////////////////////////////////////////////////
//
//		public static void calculaPareto(Populacao populacao) {
//			// TODO Auto-generated method stub
//			// n esp�os de domina��o
//			Domination S[] = Domination.createDomination(populacao.getIndividuos()
//					.size());
//			ArrayList<Integer> F = new ArrayList<Integer>();
//
//			for (int p = 0; p < populacao.getIndividuos().size(); p++) {
//				for (int q = 0; q < populacao.getIndividuos().size(); q++) {
//
//					if (p == q)
//						continue;
//					// para cada solu��o p diferente de q ; no slide i != j
//					if (populacao.getIndividuos().get(p)
//							.dominates(populacao.getIndividuos().get(q))) {
//						S[p].dominated.add(q); // sol. p dominates sol. q
//					}
//					// adiciona que o indice p domina o indice q
//					// deixa armazenado todas as solu��es que s�o dominadas por p
//					else {
//						// incrementa cada vez que acha algu�m que domina p
//						// salva quantas solu��es dominam p
//						// System.out.println(populacao.getIndividuos().get(q));
//						// System.out.println("deve dominar");
//						// System.out.println(populacao.getIndividuos().get(p));
//						if (populacao.getIndividuos().get(q)
//								.dominates(populacao.getIndividuos().get(p))) {
//							S[p].dominationCount++; // sol. q dominates sol. p
//
//						}
//
//					}
//				}
//				// Adiciona os indices de quem est� na primeira frente
//				if (S[p].dominationCount == 0) {
//					populacao.getIndividuos().get(p).setFrentePareto(1);
//					F.add(p);
//				}
//
//			}
//
//			int i = 1; // i is the front counter
//			while (!F.isEmpty()) {
//				ArrayList<Integer> Q = new ArrayList<Integer>();
//				// para cada indice em F ; na primeira itera��o s� pega os da
//				// primeira frente
//				for (Integer p : F) {
//					// vai em todas as solu��es que p domina; que s�o dominadas por
//					// p
//					for (Integer q : S[p].dominated) {
//						// decrementa a quantidade de domina��o, pois agora p est�
//						// "saindo";
//						// se a solu��o q s� era dominada por p , o .counting fica 0
//						// e q entra pra frente i + 1
//						S[q].dominationCount--;
//						if (S[q].dominationCount == 0) {
//							populacao.getIndividuos().get(q).setFrentePareto(i + 1);
//							Q.add(q);
//						}
//					}
//				}
//				i++;
//				// Q s�o os que sobram depois de remover a frente autal
//				F = Q;
//			}
//
//		}
//
//		public static void calculaDistanciaMultidao(Populacao populacao) {
//			// TODO Auto-generated method stub
//			// Linha 1 do slide; seta distancia das solu��es com 0
//			for (int i = 0; i < populacao.getIndividuos().size(); i++)
//				populacao.getIndividuos().get(i).setCrowdingDistance(0);
//
//			Collections.sort(populacao.getIndividuos(), Individuo.Comparators.PARETO);
//			int firstParetoFront = populacao.getIndividuos().get(0).getFrentePareto();
//			int lastParetoFront = populacao.getIndividuos().get(Config.TAM_POP-1).getFrentePareto();
//			int firstPos = 0, size = 0;
//
//			// para cada frente de pareto; se tiver 3 frentes i vai de 1 a 3
//			for (int i = firstParetoFront; i <= lastParetoFront; i++) {
//				// pega o tamanho da frente de pareto i
//				size = getParetoFrontSize(chromosomes, firstPos, i);
//				// cria um array de solu��es de tamanho size, nele ficar�o todas as
//				// solu��es da frente i
//				Chromosome I[] = new Chromosome[size];
//				System.arraycopy(chromosomes, firstPos, I, 0, I.length);
//
//				// se h� mais de um elemento nessa frente
//				if (size > 1) {
//					// pra cada objetivo,
//					for (int obj = 0; obj < Constants.NOBJECTIVES; obj++) {
//						// pra cada objetivo, ordena as solu��es da frente corrente
//						// com base nesse objetivo
//						// exemplo: se o objetivo corrente � pre�o, ordena os
//						// individuos por pre�o
//						Collections.sort(Arrays.asList(I),
//								FactoryComparator.getComparator(obj));
//
//						// lembrando que I � o array de solu��es dessa frente
//						// aqui os extremos est�o sendo setados como infinito
//						I[0].setCrowdingDistance(Double.POSITIVE_INFINITY);
//						I[I.length - 1]
//								.setCrowdingDistance(Double.POSITIVE_INFINITY);
//						// percorre as solu��es tirando a primeira: 0 e a ultima:
//						// length-1 ; pois tem distancia infinita
//						for (int j = 1; j < I.length - 1; j++) {
//							I[j].setCrowdingDistance(I[j].getCrowdingDistance()
//									+ I[j + 1].getObjective(obj)
//									- I[j - 1].getObjective(obj));
//						}
//					}
//
//				} else {
//					I[0].setCrowdingDistance(Double.POSITIVE_INFINITY);
//				}
//
//				firstPos += size;
//			}
//		}
//	
//}
