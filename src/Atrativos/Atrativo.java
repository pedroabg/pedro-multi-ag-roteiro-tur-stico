package Atrativos;

import java.util.ArrayList;
import java.util.List;

public class Atrativo {
	
	private int id;
	private String nome;
	private double preco;
	private double avaliacao;
	private List<TipoAtrativo> tags;
	
	
	
	public Atrativo(int id, String nome, double preco, double avaliacao,
			List<TipoAtrativo> tags) {
		super();
		this.id = id;
		this.preco = preco;
		this.avaliacao = avaliacao;
		this.nome = nome;
		this.tags = new ArrayList<TipoAtrativo>(tags);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public double getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(double avaliacao) {
		this.avaliacao = avaliacao;
	}
	public List<TipoAtrativo> getTags() {
		return tags;
	}
	public void setTags(List<TipoAtrativo> tags) {
		this.tags = tags;
	}
	
	
	
	@Override
	public String toString() {
		return "Atrativo [id=" + id + ", nome=" + nome + ", preco=" + preco
				+ ", avaliacao=" + avaliacao + ", tags=" + tags + "]\n";
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
