package Atrativos;

import java.util.ArrayList;
import java.util.List;

public class Valores {
	
	
	
	public static List<TipoAtrativo> tags = new ArrayList<TipoAtrativo>();
//	public static final Atrativo ESTACAO = new Atrativo(1, 20, 5, tags);
	
	public static List<Atrativo> ATRATIVOS = new ArrayList<Atrativo>();
	
	
	public static void init(){
		
		TipoAtrativo tPaisagem = new TipoAtrativo("Paisagem");
		TipoAtrativo tGastronomia = new TipoAtrativo("Gastronomia");
		TipoAtrativo tMusica = new TipoAtrativo("Musica");
		TipoAtrativo tMuseu = new TipoAtrativo("Museu");
		TipoAtrativo tNatureza = new TipoAtrativo("Natureza");
		TipoAtrativo tSouvenir = new TipoAtrativo("Souvenir");
		TipoAtrativo tHistoria = new TipoAtrativo("Hist�ria");
		TipoAtrativo tPraca = new TipoAtrativo("Pra�a");
		TipoAtrativo tFisico = new TipoAtrativo("Atividade F�sica");
		TipoAtrativo tIgreja = new TipoAtrativo("Igreja");
		TipoAtrativo tRomantico = new TipoAtrativo("Rom�ntico");
		TipoAtrativo tArquitetura = new TipoAtrativo("Arquitetura");
		TipoAtrativo tArte = new TipoAtrativo("Arte");
		
		
		tags.add(tPaisagem);
		tags.add(tRomantico);
		tags.add(tGastronomia);
		tags.add(tMuseu);		
		ATRATIVOS.add(new Atrativo(1, "Esta��o das Docas", 40, 5, tags));
		
		tags.clear();
		
		tags.add(tHistoria);
		tags.add(tSouvenir);
		tags.add(tGastronomia);
		tags.add(tPaisagem);
		ATRATIVOS.add(new Atrativo(2, "Ver-o-Peso", 30, 4.5, tags));
		
		tags.clear();
		
		tags.add(tPraca);		
		tags.add(tNatureza);
		tags.add(tMusica);
		tags.add(tRomantico);
		ATRATIVOS.add(new Atrativo(3, "Pra�a Batista Campos", 5, 4.0, tags));
		
		tags.clear();
		
		tags.add(tPraca);		
		tags.add(tFisico);
		tags.add(tGastronomia);
		tags.add(tRomantico);
		ATRATIVOS.add(new Atrativo(4, "Pra�a Brasil", 7, 3.8, tags));
		
		tags.clear();		
		tags.add(tHistoria);
		tags.add(tPraca);		
		tags.add(tNatureza);
		tags.add(tFisico);
		tags.add(tSouvenir);
		tags.add(tRomantico);
		ATRATIVOS.add(new Atrativo(5, "Pra�a da Rep�blica", 10, 3.5, tags));
		
		tags.clear();		
		tags.add(tHistoria);
		tags.add(tIgreja);		
		tags.add(tPraca);		
		tags.add(tMuseu);	
		ATRATIVOS.add(new Atrativo(6, "Bas�lica de Nazar�", 1, 4.5, tags));
		
		tags.clear();		
		tags.add(tNatureza);
		tags.add(tGastronomia);	
		tags.add(tPaisagem);		
		tags.add(tRomantico);		
		tags.add(tMuseu);	
		ATRATIVOS.add(new Atrativo(7, "Mangal das Gar�as", 30, 4.8, tags));
		
		tags.clear();		
		tags.add(tHistoria);		
		tags.add(tArquitetura);	
		tags.add(tPraca);	
		tags.add(tMuseu);	
		ATRATIVOS.add(new Atrativo(8, "Theatro da Paz", 10, 4.6, tags));
		
		tags.clear();		
		tags.add(tRomantico);
		tags.add(tPaisagem);	
		tags.add(tHistoria);	
		tags.add(tPraca);		
		tags.add(tMuseu);	
		ATRATIVOS.add(new Atrativo(9, "Forte do Pres�pio", 3, 4.7, tags));
		
		tags.clear();		
		tags.add(tMuseu);	
		tags.add(tIgreja);	
		tags.add(tHistoria);
		tags.add(tPraca);	
		ATRATIVOS.add(new Atrativo(10, "Museu de Arte Sacra", 2, 4.2, tags));
		
		tags.clear();		
		tags.add(tNatureza);	
		tags.add(tMuseu);	
		tags.add(tHistoria);		
		ATRATIVOS.add(new Atrativo(11, "Museu Paraense Em�lio Goeldi", 2, 4.0, tags));
		
		tags.clear();		
		tags.add(tRomantico);
		tags.add(tPaisagem);
		tags.add(tGastronomia);		
		tags.add(tMuseu);	
		tags.add(tHistoria);		
		ATRATIVOS.add(new Atrativo(12, "Casa das Onze Janelas", 2, 4.1, tags));
		
		
		tags.clear();		
		tags.add(tRomantico);
		tags.add(tMusica);		
		tags.add(tNatureza);		
		tags.add(tArte);		
		ATRATIVOS.add(new Atrativo(13, "Parque da Resid�ncia", 5, 3.5, tags));
		
		tags.clear();		
		tags.add(tArte);		
		tags.add(tHistoria);		
		tags.add(tMuseu);		
		ATRATIVOS.add(new Atrativo(14, "Espa�o S�o Jos� Liberto", 15, 3.7, tags));
		
		tags.clear();		
		tags.add(tNatureza);
		tags.add(tGastronomia);	
		tags.add(tFisico);		
		tags.add(tRomantico);		
		ATRATIVOS.add(new Atrativo(15, "Bosque Rodrigues Alves", 12, 4.1, tags));
		
		tags.clear();		
		tags.add(tGastronomia);	
		tags.add(tPaisagem);		
		tags.add(tSouvenir);		
		tags.add(tRomantico);		
		ATRATIVOS.add(new Atrativo(16, "Orla de Icoaraci", 15, 3.9, tags));
		
		
		tags.clear();		
		tags.add(tMuseu);	
		tags.add(tPaisagem);		
		
		ATRATIVOS.add(new Atrativo(17, "Corveta Solimoes", 5, 3.2, tags));
		
		
		tags.clear();		
		tags.add(tMuseu);	
		tags.add(tArte);	
		tags.add(tHistoria);		
		ATRATIVOS.add(new Atrativo(18, "Museu do C�rio de Nazar�", 2, 3.6, tags));
		
		
		
		tags.clear();		
		tags.add(tNatureza);	
		tags.add(tFisico);	
		tags.add(tPaisagem);		
		ATRATIVOS.add(new Atrativo(19, "Parque do Utinga", 5, 4.4, tags));
		
		
		
		tags.clear();		
		tags.add(tRomantico);	
		tags.add(tHistoria);	
		tags.add(tGastronomia);		
		tags.add(tMusica);		
		ATRATIVOS.add(new Atrativo(20, "Portal da Amaz�nia", 20, 3.2, tags));
//		tags.clear();		
//		tags.add(tPaisagem);	
//		tags.add(tRomantico);	
//		tags.add(tFisico);		
//		ATRATIVOS.add(new Atrativo(20, "Portal da Amaz�nia", 10, 3.2, tags));
		
		
		
		tags.clear();		
		tags.add(tMuseu);	
		tags.add(tArte);	
		tags.add(tHistoria);		
		ATRATIVOS.add(new Atrativo(21, "Museu Hist�rico do Estado do Par�", 2, 3.9, tags));
		
		
		
		Usuario.PREFERENCIAS.add(tGastronomia);
		Usuario.PREFERENCIAS.add(tRomantico);
		Usuario.PREFERENCIAS.add(tArte);
		
		
		
	}
	

}
